export const api_url = process.env.API_URL;
const proxyurl = "https://cors-anywhere.herokuapp.com/";
export const get_categories = api_url +'/wp-json/khucrabazar/api/v1/categories';
export const get_subcategories = api_url +'/wp-json/khucrabazar/api/v1/subcategory';
export const get_search_products = api_url +'/wp-json/khucrabazar/api/v1/search-products/?search=';
export const get_coverage_areas = api_url +'/wp-json/khucrabazar/api/v1/deliverable-areas';
export const add_documents = api_url +'/add-documents';
export const place_order = api_url +'/wp-json/khucrabazar/api/v1/order';
