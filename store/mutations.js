export default {
  update_product_categories(state, product_categories){
    state.product_categories = product_categories
  },
  update_selected_category(state,selected_category){
    state.selected_category=selected_category
  },
  update_product_subcategories(state, product_subcategories){
    state.product_subcategories = product_subcategories
  },
  update_product_list(state, product_list){
    state.products = product_list
  },
  add_to_card(state, item){
    state.checkout_list.push(item);
    state.checkout_products.push(item.product.productid);
  },
  remove_form_card(state, item){
    var index = state.checkout_products.indexOf(state.checkout_list[item]['product']['productid']);
    if (index !== -1) state.checkout_products.splice(index, 1);
    state.checkout_list.splice(item,1);
  },
  add_item_quantity(state, item){
    state.checkout_list[item].quantity=state.checkout_list[item].quantity+1;
  },
  reduce_item_quantity(state, item){
    if (state.checkout_list[item].quantity<=1){
      var index = state.checkout_products.indexOf(state.checkout_list[item]['product']['productid']);
      if (index !== -1) state.checkout_products.splice(index, 1);
      state.checkout_list.splice(item,1);
    }
    else {
      state.checkout_list[item].quantity=state.checkout_list[item].quantity-1;
    }
  },
  update_data_loading_state(state, data_loading_state){
    state.data_loading=data_loading_state;
  },
  update_checkout_list(state, items){
    state.checkout_list=items;
  },
  update_checkout_products(state, products){
    state.checkout_products=products;
  }
}
