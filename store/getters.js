export default {
  product_categories (state)
  {
    return state.product_categories;
  },
  selected_category (state)
  {
    return state.selected_category;
  },
  product_subcategories (state)
  {
    return state.product_subcategories;
  },
  all_products(state)
  {
    return state.products;
  },
  checkout_list(state)
  {
    return state.checkout_list;
  },
  checkout_product_list(state)
  {
    return state.checkout_products;
  },
  data_loading_sate(state){
    return state.data_loading;
  },
  order_service_charge(state){
    return state.service_charge
  }
}
