export default () => ({
  product_categories: [],
  selected_category:{},
  product_subcategories:[],
  products:[],
  checkout_list:[],
  checkout_products:[],
  service_charge: 30,
  data_loading:false
})
