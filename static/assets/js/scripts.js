(function($) {
    "use strict";

    $("#card-btn").click(function(){
      $('.order-summary').addClass('order-show');
    });
    $("#card-btn").click(function(){
      $('.overlay').addClass('add-overlay');
    });
    $("#order_summary_btn, .order-click").click(function(){
      console.log('Clicked');
      $('.order-summary').removeClass('order-show');
      $('.overlay').removeClass('add-overlay');
    });
    $(".overlay").click(function(){
      $('.order-summary').removeClass('order-show');
      $('.overlay').removeClass('add-overlay');
    });


})(jQuery); // End of use strict


// Products quantity increment and decrement
jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});
